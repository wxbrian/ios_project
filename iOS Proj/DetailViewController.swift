//
//  DetailViewController.swift
//  sms
//
//  Created by Abita Shiney on 3/7/18.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var carNamelbl: UILabel!
    
    @IBOutlet weak var carMakelbl: UILabel!
    
    @IBOutlet weak var carcolorlbl: UILabel!
    
    @IBOutlet weak var lanelbl: UILabel!
    
    @IBOutlet weak var spotNumlbl: UILabel!
    
    @IBOutlet weak var payTypelbl: UILabel!
    
    @IBOutlet weak var pricelbl: UILabel!
    
    @IBOutlet weak var timelbl: UILabel!
    
    
    var carnumDisplay = ""
    var modelDisplay = ""
    var colorDisplay = ""
    var laneDisplay = ""
    var spotDisplay = ""
    var payDisplay = ""
    var priceDisplay = ""
    var timeDisplay = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carNamelbl.text = carnumDisplay
        carMakelbl.text = modelDisplay
        carcolorlbl.text = colorDisplay
        lanelbl.text = laneDisplay
        spotNumlbl.text = spotDisplay
        payTypelbl.text = payDisplay
        pricelbl.text = priceDisplay
        timelbl.text = timeDisplay
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
            }
    

}
