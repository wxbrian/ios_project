//
//  tickets.swift
//  iOS Proj
//
//  Created by Admin on 2018-03-08.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import Foundation

class AddTicket
{
    var carNumber: String
    var carName: String
    var carColor: String
    var myImage: String
    var parkLane: String
    var spotNum: String
    var payMethod: String
    var date:String
    var price:String
    
    var timeDisplay:String
    
    private static var tickets = [AddTicket]()
    
    static func addTicket(ticket:AddTicket) {
        tickets.append(ticket)
    }
    
    static func countTickets() -> Int {
        return tickets.count
    }
    
    static func getTicket(index:Int) -> AddTicket {
        return tickets[index]
    }
    
    init(carColor: String, carName: String,carNumber: String, parkLane: String,
         payMethod: String, price:String, myImage:String, date:String, timeDisplay:String, spotNum:String)
    {
        self.carNumber = carNumber
        self.carName = carName
        self.carColor = carColor
        self.parkLane = parkLane
        self.spotNum = spotNum
        self.payMethod = payMethod
        self.myImage = myImage
        self.timeDisplay = timeDisplay
        
        self.date = date
        self.price = price
    }
}
