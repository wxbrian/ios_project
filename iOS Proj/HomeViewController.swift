//
//  HomeViewController.swift
//  iOS Proj
//
//  Created by Admin on 2018-03-08.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var myReportCount: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        myReportCount.text = String(AddTicket.countTickets())
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
