//
//  RegisterViewController.swift
//  iOS Proj
//
//  Created by Abita Shiney on 2/23/18.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    @IBOutlet weak var btnReg: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func bttnRegister(_ sender: UIButton) {

        
        if ((txtName.text?.isValidName(testStr: txtName.text!) == false)) {
            let alert  =
                UIAlertController(title: "Okey", message: "Name not valid", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
            
        } else if ((txtEmail.text?.isValidEmail(testStr: txtEmail.text!) == false)){
            let alert  =
                UIAlertController(title: "Okey", message: "Email not valid", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
        } /* else if ((txtContNum.text?.isValidNumber(testStr: txtContNum.text!) == false)){
            let alert  =
                UIAlertController(title: "Okey", message: "Phone not valid, try xxx-xxx-xxxx", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
        } */
        else {
            
        var user = RegisterData()
        user.userEmail = txtEmail.text!
        user.userPassword = txtPassword.text!
        user.userContactNumber = ""
        user.userName = txtName.text!
        user.userCarPlate = ""
        
        let flag = RegisterData.addUser(usr: user)
        if flag == true{
            let alert  =
                UIAlertController(title: "Okey", message: "User registered successfully. 👏🏻", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { _ -> Void in
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! ViewController
                nextViewController.user = user
                self.present(nextViewController, animated: true, completion: nil)
            })
            
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert  =
                UIAlertController(title: "Error", message: "It seems this user already exists. Try to log in", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}
