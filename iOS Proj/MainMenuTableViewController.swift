//
//  MainMenuTableViewController.swift
//  iOS Proj
//
//  Created by sakshi singh on 01/03/18.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class MainMenuTableViewController: UITableViewController{

    @IBOutlet weak var logOut: UILabel!
    @IBOutlet weak var profileUpdate: UILabel!

    @IBOutlet weak var welcomeUser: UILabel!
    
    var user:RegisterData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if user != nil {
        welcomeUser.text = ("Welcome, \(user!.userName)")
        }
        let tapLogOut = UITapGestureRecognizer(target: self, action: #selector(MainMenuTableViewController.tapLogOut))
        logOut.isUserInteractionEnabled = true
        logOut.addGestureRecognizer(tapLogOut)
        
        let tapProfileUpdate = UITapGestureRecognizer(target: self, action: #selector(MainMenuTableViewController.tapProfileUpdate))
        profileUpdate.isUserInteractionEnabled = true
        profileUpdate.addGestureRecognizer(tapProfileUpdate)

      }
    
    @objc func tapLogOut(sender:UITapGestureRecognizer) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! ViewController
        self.present(nextViewController, animated: true, completion: nil)
        
    }
    
    @objc func tapProfileUpdate(sender:UITapGestureRecognizer) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "UpdateVC") as! UpdateViewController
        nextViewController.currentUser = user
        self.present(nextViewController, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Section : \(indexPath.section) Row: \(indexPath.row)")
    }
    
    
}

