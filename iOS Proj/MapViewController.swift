//
//  ViewController.swift
//  userLocation
//
//  Created by sakshi singh on 07/03/18.
//  Copyright © 2018 sakshi. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class MapViewController: UIViewController, CLLocationManagerDelegate{
    
    
    @IBOutlet var map: MKMapView!
    let locationmanager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        locationmanager.delegate = self
        locationmanager.desiredAccuracy = kCLLocationAccuracyBest
        locationmanager.requestWhenInUseAuthorization()
        locationmanager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations[0]
        let center = location.coordinate
        
        let span = MKCoordinateSpanMake(0.01, 0.01)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(center, span)
        map.setRegion(region, animated: true)
        
        print(location.altitude)
        print(location.speed)
        
        map.showsUserLocation = true
    }
    
    
    
    
    
    
}

