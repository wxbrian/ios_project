//
//  RegisterData.swift
//  iOS Proj
//
//  Created by Admin on 2018-02-26.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import Foundation

class RegisterData{

    var userName: String
    var userEmail: String
    var userPassword: String
    var userContactNumber: String
    var userCarPlate: String
    
    private static var userList = [String:RegisterData]()
    
    init(){
        self.userName = ""
        self.userEmail = ""
        self.userPassword = ""
        self.userContactNumber = ""
        self.userCarPlate = ""
        
    }
    
    init(_ userName: String, _ userEmail: String , _ userPassword: String, _ userContactNumber: String, _ userCarPlate: String){
        self.userName = userName
        self.userEmail = userEmail
        self.userPassword = userPassword
        self.userContactNumber = userContactNumber
        self.userCarPlate = userCarPlate
    }
    
    static func addUser(usr: RegisterData) -> Bool{
        
        if self.userList[usr.userEmail] == nil{
            self.userList[usr.userEmail] = usr
            return true
        }
        return false
    }
    
    static func getAllUsers() -> [String:RegisterData] {
        return userList
    }
    
    static func getUserByEmail(userEmail: String) -> RegisterData {
        
        if self.userList[userEmail] != nil{
            return self.userList[userEmail]!
        }
        return RegisterData()
    }
    
    static func updateUserByEmail(usr: RegisterData) -> Bool{
        
        if self.userList[usr.userEmail] != nil{
            self.userList[usr.userEmail] = usr
            return true
        }
        return false
    }
    
    
    
}

