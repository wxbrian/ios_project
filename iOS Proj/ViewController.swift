//
//  ViewController.swift
//  iOS Proj
//
//  Created by Abita Shiney on 2/22/18.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class ViewController: UIViewController, BEMCheckBoxDelegate {

    @IBOutlet weak var rememberMe: UILabel!
    @IBOutlet weak var CheckBoxRemember: BEMCheckBox!

    var myUserDefault: UserDefaults!
    var user: RegisterData?
    
    @IBOutlet weak var myVersion: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnSignUp: UIButton!
    
    let usersList = RegisterData.getAllUsers()
    
        override func viewDidLoad() {
        super.viewDidLoad()

            myUserDefault = UserDefaults.standard
            
            if let userName = myUserDefault.value(forKey: "userName"){
                txtEmail.text = userName as? String
            }
            
            if let userPassword = myUserDefault.value(forKey: "userPassword"){
                txtPassword.text = userPassword as? String
            }
            
            if user != nil {
                txtEmail.text = user?.userEmail
                txtPassword.text = user?.userPassword
                
            }

        var versionComplete = ""
            
            if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") {
                versionComplete += "V \(version)"
            }
            if let versionCode = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") {
                versionComplete += "(\(versionCode))"
            }
            
            myVersion.text = versionComplete
            rememberMe.text = "Remember me"

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didTapRememberMe(_ sender: BEMCheckBox) {
        print(CheckBoxRemember.on)
        
    }
    @IBAction func loginButton(_ sender: UIButton) {
        
        if ((txtEmail.text == "") || (txtPassword.text == "")) {
        
            let alert  =
                UIAlertController(title: "Error", message: "One or more Fields are empty", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
            
        } else if usersList.isEmpty == true {
            
            let alert  =
                UIAlertController(title: "Error", message: "You might want to create an account", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
            
        }else {
            
        for (_, value) in usersList{
            
            if (value.userEmail == txtEmail.text && value.userPassword == txtPassword.text){

            if self.CheckBoxRemember.on{
                self.myUserDefault.set(txtEmail.text, forKey: "userName")
                self.myUserDefault.set(txtPassword.text, forKey: "userPassword")
            }else {
                self.myUserDefault.removeObject(forKey: "userName")
                self.myUserDefault.removeObject(forKey: "userPassword")
            }
            
                let myStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextVC = myStoryBoard.instantiateViewController(withIdentifier: "MainVC") as! MainMenuTableViewController
                nextVC.user = user
                self.present(nextVC, animated: true, completion: nil)
            } else {
                
                let alert  =
                    UIAlertController(title: "Error", message: "User Email / Password incorrect", preferredStyle: UIAlertControllerStyle.alert)
                
                let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                alert.addAction(actionOk)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
}
    
    func verifyLogin(_ email: String,_ password: String) {
                if(txtEmail.text == email && txtPassword.text == password)
                {
                    let myStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let nextVC = myStoryBoard.instantiateViewController(withIdentifier: "homeScreen") 
                    self.present(nextVC, animated: true, completion: nil)
                }
                else{
                    let alert  =
                        UIAlertController(title: "Error", message: "User Email / Password incorrect", preferredStyle: UIAlertControllerStyle.alert)
                   
                    let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                    alert.addAction(actionOk)
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }

    }


