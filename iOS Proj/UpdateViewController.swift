//
//  UpdateViewController.swift
//  iOS Proj
//
//  Created by Admin on 2018-03-07.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class UpdateViewController: UIViewController {

    @IBOutlet weak var myName: UITextField!
    @IBOutlet weak var myEmail: UITextField!
    @IBOutlet weak var myPassword: UITextField!
    @IBOutlet weak var myNumber: UITextField!
    @IBOutlet weak var myCarPlate: UITextField!
    
    var currentUser:RegisterData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        myEmail.text = currentUser?.userEmail
        myEmail.isUserInteractionEnabled = false
    }

    @IBAction func updateUser(_ sender: UIButton) {
    
    
        if ((myName.text?.isValidName(testStr: myName.text!) == false)) {
            let alert  =
                UIAlertController(title: "Okey", message: "Name not valid", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
            
        } else if ((myEmail.text?.isValidEmail(testStr: myEmail.text!) == false)){
            let alert  =
                UIAlertController(title: "Okey", message: "Email not valid", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
        } else if ((myNumber.text?.isValidNumber(testStr: myNumber.text!) == false)){
             let alert  =
             UIAlertController(title: "Okey", message: "Phone not valid, try xxx-xxx-xxxx", preferredStyle: UIAlertControllerStyle.alert)
             
             let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
             alert.addAction(actionOk)
             
             self.present(alert, animated: true, completion: nil)
             }
        else {
            
            var user = RegisterData()
            user.userEmail = myEmail.text!
            user.userPassword = myPassword.text!
            user.userContactNumber = myNumber.text!
            user.userName = myName.text!
            user.userCarPlate = myCarPlate.text!
            
            let flag = RegisterData.updateUserByEmail(usr: user)
            if flag == true{
                let alert  =
                    UIAlertController(title: "Okey", message: "User updated successfully. 👏🏻", preferredStyle: UIAlertControllerStyle.alert)
                
                let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { _ -> Void in
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MainVC") as! MainMenuTableViewController
                    nextViewController.user = user
                    self.present(nextViewController, animated: true, completion: nil)
                })
                alert.addAction(actionOk)
                
                self.present(alert, animated: true, completion: nil)
            }else{
                let alert  =
                    UIAlertController(title: "Error", message: "We couldn`t update ur profile, sorry 😭", preferredStyle: UIAlertControllerStyle.alert)
                
                let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                alert.addAction(actionOk)
                
                self.present(alert, animated: true, completion: nil)
            }
            
            
        }
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "backToMain", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVC = segue.destination as! MainMenuTableViewController
        destVC.user = currentUser
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
