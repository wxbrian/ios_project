//
//  TicketTableViewCell.swift
//  iOS Proj
//
//  Created by Admin on 2018-03-08.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class TicketTableViewCell: UITableViewCell {
    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var CarName: UILabel!
    @IBOutlet weak var CarNumber: UILabel!
    @IBOutlet weak var carColor: UILabel!
    @IBOutlet weak var parkLane: UILabel!
    @IBOutlet weak var payMethod: UILabel!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var timeDisplay: UILabel!
    @IBOutlet weak var spotNum: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
