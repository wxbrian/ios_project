//
//  utils.swift
//  iOS Proj
//
//  Created by Admin on 2018-03-02.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import Foundation

extension String {
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidName(testStr:String) -> Bool {
        let nameRegEx = "^[\\p{L}\\s'.-]+$"
        
        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: testStr)
    }
    
    func isValidNumber(testStr:String) -> Bool {
        let numberRegEx = "\\D*([2-9]\\d{2})(\\D*)([2-9]\\d{2})(\\D*)(\\d{4})\\D*"
        
        let numberTest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return numberTest.evaluate(with: testStr)
    }
}
