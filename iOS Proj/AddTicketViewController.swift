//
//  AddTicketViewController.swift
//  iOS Proj
//
//  Created by Abita Shiney on 3/2/18.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class AddTicketViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate
{
   
    @IBOutlet weak var mySegment: UISegmentedControl!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var carNum: UITextField!
    
    @IBOutlet weak var carName: UITextField!
    
    @IBOutlet weak var carColor: UITextField!
    
    @IBOutlet weak var myImage: UIImageView!
    
    @IBOutlet weak var parkLane: UITextField!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var spotNum: UITextField!
    
    @IBOutlet weak var payMethod: UITextField!
    
    var ticketList:AddTicket?
    
    @IBAction func segmentAction(_ sender: Any) {
        if(mySegment.selectedSegmentIndex == 0){
            priceLabel.text = "$5"
        }
        else if(mySegment.selectedSegmentIndex == 1){
            priceLabel.text = "$10"
        }
        else if(mySegment.selectedSegmentIndex == 2){
            priceLabel.text = "$15"
        }
        else if(mySegment.selectedSegmentIndex == 3){
            priceLabel.text = "$20"
        }
    }
    
    
    @IBAction func savePressed(_ sender: UIButton) {
        let ticketList = AddTicket(
        carColor: carColor.text!,
        carName: carName.text!,
        carNumber: carNum.text!,
        parkLane: parkLane.text!,
        payMethod: payMethod.text!,
        price: priceLabel.text!,
        myImage: ("\(carName.text!).png"),
        date: lblDate.text!,
        timeDisplay: mySegment.titleForSegment(at: mySegment.selectedSegmentIndex)!,
        spotNum:spotNum.text!)

        AddTicket.addTicket(ticket:ticketList)
        
        performSegue(withIdentifier: "showdata", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVC = segue.destination as! DetailViewController
        destVC.carnumDisplay = carNum.text!
        destVC.modelDisplay = carName.text!
        destVC.colorDisplay = carColor.text!
        destVC.laneDisplay = parkLane.text!
        destVC.spotDisplay = spotNum.text!
        destVC.payDisplay = payMethod.text!
        destVC.priceDisplay = priceLabel.text!
        let title = mySegment.titleForSegment(at: mySegment.selectedSegmentIndex)
       destVC.timeDisplay = title!
    }
    
    
    var currentSelected: Int = 0
    var logoImage: String!
    var myPickerView: UIPickerView!
     

    var CarMake = ["BMW","Jaquar","Audi","Bugatti","Ferrari","Chrysler","Mercedes-Benz","Acura"]
    var CarColors = ["Red","Black","White","Brown","Silver","Gray","Blue"]
    
    var Lanelist = ["Lane A","Lane B","Lane C","Lane D","Lane E"]
    
    var SpotList = ["P1","P2","P3","P4","P5","P6","P7","P8"]
    
    var PaymentList = ["Credit Card", "Debit Card", "Master Card", "Cash"]
    
    override func viewDidLoad() {
       super.viewDidLoad()
            getCurrentDateTime()
            self.carName.delegate = self
            self.carColor.delegate = self
            self.parkLane.delegate = self
            self.spotNum.delegate = self
            self.payMethod.delegate = self
        
       
    }
    
    func getCurrentDateTime(){
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .medium
        let str = formatter.string(from: Date())
        lblDate.text = str
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
 
    func pickUp(_ textField : UITextField){
        
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
      self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
       self.myPickerView.backgroundColor = UIColor.white
       textField.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(AddTicketViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(AddTicketViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }

   func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if currentSelected == 1
        {
        return CarMake.count
        }
        else if currentSelected == 2
        {
            return CarColors.count
        }
        else if currentSelected == 3
        {
            return Lanelist.count
        }
        else if currentSelected == 4
        {
            return SpotList.count
        }
        else {
            return PaymentList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if currentSelected == 1{
            self.carName.text = CarMake[row]
            myImage.image = UIImage(named:"\(carName.text!).png")
            print(carName.text!)
        }
        else if currentSelected == 2{
                self.carColor.text = CarColors[row]
            
        }
        else if currentSelected == 3
        {
            self.parkLane.text = Lanelist[row]
           
        }
        else if currentSelected == 4
        {
            self.spotNum.text = SpotList[row]
        }
        else if currentSelected == 5
        {
            self.payMethod.text = PaymentList[row]
        }
    
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if currentSelected == 1{
        return CarMake[row]
        }
        else if currentSelected == 2{
        return CarColors[row]
    }
        else if currentSelected == 3
        {
            return Lanelist[row]
        }
        else if currentSelected == 4
        {
            return SpotList[row]
        }
        else
        {
            return PaymentList[row]
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(carName)
        {
            currentSelected = 1
            self.pickUp(textField)
            myImage.image = UIImage(named:"\(carName).png")
        }
        else if textField.isEqual(carColor){
            currentSelected = 2
            self.pickUp(textField)
        }
        else if textField.isEqual(parkLane){
            currentSelected = 3
            self.pickUp(textField)
        }
        else if textField.isEqual(spotNum){
            currentSelected = 4
            self.pickUp(textField)
        }
        else if textField.isEqual(payMethod){
            currentSelected = 5
            self.pickUp(textField)
        }

    }
    
    @objc func doneClick() {
                if currentSelected == 1
        {
        carName.resignFirstResponder()
        
        }
                else if currentSelected == 2
        {
            carColor.resignFirstResponder()
            
        }
                else if currentSelected == 3
                {
                    parkLane.resignFirstResponder()
                    
        }
                else if currentSelected == 4
                {
                    spotNum.resignFirstResponder()
                    
        }
                else if currentSelected == 5
                {
                    payMethod.resignFirstResponder()
        }
    }
    @objc func cancelClick() {
        if currentSelected == 1
        {
       carName.resignFirstResponder()
        }
        else if currentSelected == 2
        {
            carColor.resignFirstResponder()
    }
        else if currentSelected == 3
        {
            parkLane.resignFirstResponder()
        }
        else if currentSelected == 4
        {
            spotNum.resignFirstResponder()
        }
        else if currentSelected == 5
        {
            payMethod.resignFirstResponder()
        }
}
}
