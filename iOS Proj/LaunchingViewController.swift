//
//  HomeViewController.swift
//  iOS Proj
//
//  Created by Admin on 2018-02-23.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class LaunchingViewController: UIViewController {

    @IBOutlet weak var myProggressBar: UIProgressView!

    var timer = Timer()
    var seconds = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector (showProgresss), userInfo: nil, repeats: true)
        myProggressBar.transform = myProggressBar.transform.scaledBy(x: 1, y: 3)

    }

    @objc func showProgresss()
    {
        
        seconds -= 1
        myProggressBar.progress += 0.10
        if(myProggressBar.progress == 1){
        let myStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = myStoryBoard.instantiateViewController(withIdentifier: "navVC") 
        self.present(nextVC, animated: true, completion: nil)
        }

    }
   
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
