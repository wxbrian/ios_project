//
//  InstructionViewController.swift
//  iOS Proj
//
//  Created by MacStudent on 2018-02-27.
//  Copyright © 2018 Abita Shiney. All rights reserved.
//

import UIKit

class InstructionViewController: UIViewController {

    @IBOutlet weak var myWebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadfromFile()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadfromFile()
    {
        let localfilePath = Bundle.main.url(forResource: "instruction", withExtension: "html");
        let myRequest = NSURLRequest(url: localfilePath!);
        myWebView.loadRequest(myRequest as URLRequest); 
    }



}
